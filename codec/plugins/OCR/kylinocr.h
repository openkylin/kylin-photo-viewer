#ifndef KYLINOCRCLASS_H
#define KYLINOCRCLASS_H

#include <QObject>
#include "ocr.h"
class KylinOCR : public OCR
{
    Q_OBJECT
public:
    KylinOCR();
    void getText(QString) override;
    //    void getRect(QString imagPath) override
    //    {
    //        Q_UNUSED(imagPath);
    //    }
};

#endif // KYLINOCRCLASS_H
