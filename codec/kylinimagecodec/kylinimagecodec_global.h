#ifndef KYLINIMAGECODEC_GLOBAL_H
#define KYLINIMAGECODEC_GLOBAL_H

#include <QtCore/qglobal.h>

#include <QDebug>

#include <QFileInfo>
#include <QPainter>
#include <QSvgRenderer>
#include <QSvgGenerator>
#include <QMovie>
#include <QPixmap>
#include <QHash>
#include <stb/stb_image_write.h>
#include <stb/stb_image.h>
#include <gif_lib.h>
#include <FreeImage.h>
#include <FreeImagePlus.h>
#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/imgproc.hpp>
using namespace cv;

#if defined(KYLINIMAGECODEC_LIBRARY)
#define KYLINIMAGECODEC_EXPORT Q_DECL_EXPORT
#else
#define KYLINIMAGECODEC_EXPORT Q_DECL_IMPORT
#endif

#endif // KYLINIMAGECODEC_GLOBAL_H
